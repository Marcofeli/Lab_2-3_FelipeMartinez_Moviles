package com.example.f1419.lab2_3_felipemartinez;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnInfoWindowClickListener {

    private GoogleMap mMap;
    private BDHelper helper;
    private ArrayList<Sede> sedes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        this.helper = new BDHelper(this);
        this.helper.insertarDatos();
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        // mueve la cámara a CR
        LatLng cr = new LatLng(9.0583995,-83.9660188);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(cr,7));
        this.sedes = this.helper.getSedes();
        for (Sede sede : sedes) {
            mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(Float.parseFloat(sede.getLat()),Float.parseFloat(sede.getLng())))
                    .title(sede.getNombre())
            );
        }
        mMap.setOnInfoWindowClickListener(this);

    }

    public AlertDialog createSingleListDialog(Sede sede) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle(sede.getNombre());
        builder.setMessage(
                        "Descripción: "+sede.getDescripcion()+"\n\n"+
                        "Latidud: "+ sede.getLat()+ "\n"+
                        "Longitud: "+ sede.getLng()
        );

        return builder.create();
    }

    @Override
    public void onInfoWindowClick(Marker marker) {

        for (Sede sede: this.sedes ) {
            if (sede.getNombre().equals(marker.getTitle())){
                AlertDialog dialog = createSingleListDialog(sede);
                dialog.show();
            }
        }
    }

}
