package com.example.f1419.lab2_3_felipemartinez;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

/**
 * Created by f1419 on 9/5/2018.
 */

public class BDHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "MiBD.db";
    public static final String TABLE_NAME = "sede";
    public static final String COLUMN_NAME = "nombre";
    public static final String COLUMN_DESCRIPTION = "descripcion";
    public static final String COLUMN_LAT = "lat";
    public static final String COLUMN_LNG = "lng";

    public BDHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("create table " + TABLE_NAME +
                "(" + COLUMN_NAME + " text primary key,"
                    + COLUMN_DESCRIPTION + " text, "
                    + COLUMN_LAT + " text, "
                    + COLUMN_LNG +" text)");
    }

    // 9.8564963,-83.9125516 Cartago
    // 10.1026622,-83.6893576 Limon
    // 10.3652482,-84.5057934 SC
    // 9.9378868,-84.0755203 Barrio Amón
    // 10.0198026,-84.1971997 Alajuela

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("drop table if exists " + TABLE_NAME);
        onCreate(sqLiteDatabase);
    }

    public boolean insertarSede(String nombre, String descripcion, String lat, String lng){
        SQLiteDatabase bd = this.getWritableDatabase();
        ContentValues registro = new ContentValues();
        registro.put(COLUMN_NAME, nombre);
        registro.put(COLUMN_DESCRIPTION, descripcion);
        registro.put(COLUMN_LAT, lat);
        registro.put(COLUMN_LNG, lng);
        bd.insert(TABLE_NAME, null, registro);
        bd.close();
        return true;
    }

    public ArrayList<Sede> getSedes(){
        String selectQuery = "SELECT  * FROM " + TABLE_NAME;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        ArrayList<Sede> arrayListData = new ArrayList<Sede>();
        if (cursor.moveToFirst()){
            do {
                Sede sede = new Sede();
                sede.setNombre(cursor.getString(0));
                sede.setDescripcion(cursor.getString(1));
                sede.setLat(cursor.getString(2));
                sede.setLng(cursor.getString(3));
                arrayListData.add(sede);
            } while (cursor.moveToNext());
        }
        return arrayListData;
    }

    public void insertarDatos(){
        this.insertarSede("Cartago","Sede central del TEC, aquí fue el SpaceApps","9.8564963","-83.9125516");
        this.insertarSede("San Carlos","Sede norte del TEC, aquí hay cocodrilos","10.3652482","-84.5057934");
        this.insertarSede("Limón","Sede caribe del TEC, aquí se habla patuá","10.1026622","-83.6893576");
        this.insertarSede("Barrio Amón","Sede en barrio Amón, aquí está la casa verde","9.9378868","-84.0755203");
        this.insertarSede("Alajuela","Sede en Alajuela, aquí creo que no se dán clases","10.0198026","-84.1971997");
    }

}
